# votifier_py

A Python implementation of the Minecraft Votify/NuVotify projects.

Acts as a votifier server; receiving, unpacking and interpreting incoming votes asynchronously from voting websites for all your pythonic backend needs. Think [Vanilla Votifier](https://github.com/xMamo/VanillaVotifier) but as a Python library.

Currently supports the original Votifier protocol as well as Votifier2.

**Note**: Expect breaking changes until people pester me with issues. :)
